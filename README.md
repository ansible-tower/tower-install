Ansible Tower HA Install on AWS
=========

This playbook deploys the following in AWS:

* Three node Tower Cluster
* Postgresql RDS DB instance
* Elastic LoadBalancer to front Tower (optional)
* Create route53 CNAME for Loadbalancer (optional)
* Deploy custom certs (optional)
* Private Automation Hub (optional)

Requirements
------------

This playbook deploys to AWS so needs python boto, boto3 and botocore modules. Also needs AWS credentials.

Playbook Variables
--------------

Variables in group_vars/all need to be updated according to your deployment.
