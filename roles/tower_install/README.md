tower install role
=========

This role installs tower by:

* grabbing the tower installer
* configuring sshkeys for deploying across the cluster
* deploys automation hub (optional)

Requirements
------------

This playbook deploys to AWS so needs python boto, boto3 and botocore modules. Also needs AWS credentials.

Playbook Variables
--------------

The following variables are set in defaults/main.yml and can be changed as needed.

* **tower_version:** Tower version to install - 3.7.X.X or 3.8.X.X should work. I tend to leave it as latest though.
