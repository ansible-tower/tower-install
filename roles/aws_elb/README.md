aws elb deploy role
=========

This role deploys an elb to front Tower. Only gets create if the **deploy_elb** variable is true.

Requirements
------------

This playbook deploys to AWS so needs python boto, boto3 and botocore modules. Also needs AWS credentials.

Playbook Variables
--------------

The following variables are set in group_vars/all in the root of the repository and can be changed as needed.

* **elb_name:** name of new elb instance
