aws route53 role
=========

This role creates a dns record in route53 - a CNAME for the ELB instance. Only gets created when the **tower_dns** and **deploy_elb** variables are true.

Requirements
------------

This playbook deploys to AWS so needs python boto, boto3 and botocore modules. Also needs AWS credentials.

Playbook Variables
--------------

The following variables are set in group_vars/all in the root of the repository and can be changed as needed.

* **tower_dns:** whether to configure DNS record
* **dns_zone:** dns zone in route53 e.g. example.com
* **tower_hostname:** dns record to create in zone e.g. tower-ha would result in tower-ha.example.com

