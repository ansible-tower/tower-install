find ami role
=========

This role finds AMI image id per ec2 region.

Requirements
------------

This playbook deploys to AWS so needs python boto, boto3 and botocore modules. Also needs AWS credentials.

Playbook Variables
--------------

The following variables are set in defaults/main.yml and can be changed as needed.

* **instance_info:** list of operating systems to find OS for. Default is rhel8.
