aws deploy ec2 instances
=========

This role deploys ec2 instances across 3 different AZ's in region.

Requirements
------------

This playbook deploys to AWS so needs python boto, boto3 and botocore modules. Also needs AWS credentials.

Playbook Variables
--------------

The following variables are set in defaults/main.yml and can be changed as needed.

* **instance_size:** instance size
* **os:** operating system used to find AMI ID. Defaults to rhel8.
