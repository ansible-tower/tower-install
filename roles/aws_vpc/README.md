aws vpc role
=========

This role deploys the following network components in ec2:

* vpc
* subnets in 3 AZ's
* routing table
* security group
* Deploy custom certs (optional)

Requirements
------------

This playbook deploys to AWS so needs python boto, boto3 and botocore modules. Also needs AWS credentials.

Playbook Variables
--------------

The following variables are set in defaults/main.yml and can be changed as needed.

* **vpc_name:** name of new vpc that will be created
* **vpc_cidr:** network cidr for vpc
* **subnets:** list of subnets per az
* **security_group:** name of security group to create
* **security_group_rules:** list of security group rules
