aws deploy rds role
=========

This role deploys the postgresql rds DB instance.

* rds subnet
* rds instance
* waits for instance to come online

Requirements
------------

This playbook deploys to AWS so needs python boto, boto3 and botocore modules. Also needs AWS credentials.

Playbook Variables
--------------

The following variables are set in defaults/main.yml and can be changed as needed.

* **rds_subnet_name:** rds subnet name
* **rds_subnet_description:** rds subnet description
* **rds_instance_name:** rds instance name
* **rds_db_name:** DB name
* **rds_db_engine:** DB engine (postgresql)
* **rds_db_license_model:** license model
* **rds_engine_version:** postgresql version (Tower latest needs 10.6)
* **rds_db_size:** size of DB disk
* **rds_instance_size:** instance size
* **rds_db_user:** user account (needed for Tower installation)
* **rds_db_password:** password (needed for Tower installation)
* **rds_backup_retention:** DB backups (disabled by default)
